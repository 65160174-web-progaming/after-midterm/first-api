import { PartialType } from '@nestjs/swagger';
import { CreateUserDto } from './create-user.dto';

export class UpdateUserDto extends PartialType(CreateUserDto) {
  login: string;
  password: string;
  roles: ('admin' | 'user')[];
  gender: 'male' | 'female';
  age: number;
}
