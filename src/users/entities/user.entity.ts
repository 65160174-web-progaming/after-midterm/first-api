import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class User {
  @PrimaryGeneratedColumn()
  id: number;
  @Column()
  login: string;
  @Column()
  password: string;
  @Column('json', { default: 'User' })
  roles: string[];
  @Column({ default: 'male' })
  gender: 'male' | 'female';
  @Column()
  age: number;
}
