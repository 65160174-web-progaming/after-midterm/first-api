import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { User } from './entities/user.entity';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(User)
    private readonly userRepository: Repository<User>,
  ) {}

  // lastId: number = 1;
  // users: User[] = [
  //   {
  //     id: 1,
  //     login: 'admin',
  //     password: 'Pass1234',
  //     roles: ['admin'],
  //     gender: 'male',
  //     age: 20,
  //   },
  // ];

  async create(createUserDto: CreateUserDto): Promise<User> {
    const newUser: User = { ...createUserDto };
    const createdUser = await this.userRepository.save(newUser);
    return createdUser;
  }


  async findAll(): Promise<User[]> {
    return this.userRepository.find();
  }

  async findOne(id: number): Promise<User> {
  const user = await this.userRepository.findOne({ where: { id: id } });
  if (!user) {
    throw new NotFoundException();
  }
  return user;
}

   async update(id: number, updateUserDto: UpdateUserDto): Promise<User> {
    const user = await this.findOne(id);
    const updatedUser = await this.userRepository.save({ ...user, ...updateUserDto });
    return updatedUser;
  }

  async remove(id: number): Promise<User> {
    const user = await this.findOne(id);
    await this.userRepository.remove(user);
    return user;
  }

}
